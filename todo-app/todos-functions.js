// Get user saved todos from localStorage
const getSavedTodos = function () {
	const todosJSON = localStorage.getItem("todos");

	if (todosJSON !== null) {
		return JSON.parse(todosJSON);
	} else {
		return [];
	}
};
const saveTodos = function (todos) {
	return localStorage.setItem("todos", todos);
};

//Render application todos based on filters
const renderTodos = (todos, filters) => {
	const filteredTodos = todos.filter((todo) => {
		if (filters.hideCompleted) {
			return (
				todo.title.toLowerCase().includes(filters.searchText.toLowerCase()) &&
				!todo.completed
			);
		} else if (!filters.hideCompleted) {
			return todo.title
				.toLowerCase()
				.includes(filters.searchText.toLowerCase());
		}
	});
	const incompleteTodos = filteredTodos.filter((todo) => {
		return !todo.completed;
	});

	document.querySelector("#todos").innerHTML = "";

	getTodoSummary(incompleteTodos);

	filteredTodos.forEach((todo) => {
		document.querySelector("#todos").appendChild(generateTodoDOM(todo));
	});
};

// Get the DOM elements for an individual note
const generateTodoDOM = function (data) {
	const todoEl = document.createElement("p");
	todoEl.textContent = data.title;
	return todoEl;
};

// Get summary for todos
const getTodoSummary = function (incompleteTodos) {
	const summary = document.createElement("h2");
	summary.textContent = `You have ${incompleteTodos.length} todos left`;
	document.querySelector("#todos").appendChild(summary);
};

let todos = getSavedTodos();

const filters = {
	searchText: "",
	hideCompleted: false,
};

const form = document.querySelector("#todos-form");

// Event Listeners

const checkbox = document.querySelector("#todos-checkbox");
checkbox.addEventListener("change", (e) => {
	filters.hideCompleted = e.target.checked;
	renderTodos(todos, filters);
});

renderTodos(todos, filters);

document.querySelector("#search-text").addEventListener("input", function (e) {
	filters.searchText = e.target.value;
	renderTodos(todos, filters);
});

form.addEventListener("submit", (e) => {
	e.preventDefault();
	todos.push({ title: e.target.elements.todoData.value, completed: false });
	let todosJSON = JSON.stringify(todos);
	saveTodos(todosJSON);
	renderTodos(todos, filters);
	e.target.elements.todoData.value = "";
});

let notes = getSavedNotes();

const filters = {
	searchText: "",
};

renderNotes(notes, filters);

const addBtn = document.querySelector("#add-note-btn");
const input = document.querySelector("#my-input");

const dropDown = document.querySelector("#filter-by");

/* -------------------- Event Listeners  -------------------- */

addBtn.addEventListener("click", (e) => {
	notes.push({
		title: "",
		body: "",
	});

	localStorage.setItem("notes", JSON.stringify(notes));
	renderNotes(notes, filters);
});

input.addEventListener("input", (e) => {
	document.querySelector("#notes").textContent = "";
	const inputValue = e.target.value;
	filters.searchText = inputValue;
	renderNotes(notes, filters);
});

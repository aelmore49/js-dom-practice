// Check for existing save user data
const getSavedNotes = function () {
	const notesJSON = localStorage.getItem("notes");
	if (notesJSON !== null) {
		return JSON.parse(notesJSON);
	} else {
		return [];
	}
};

// Generate the DOM structure for a note
const generateNoteDOM = function (note) {
	const noteEl = document.createElement("p");
	noteEl.textContent = note.title.length > 0 ? note.title : "Unamed Note";
	return noteEl;
};

// Render application notes
const renderNotes = (notes, filters) => {
	const filteredNotes = notes.filter((note) => {
		return note.title.toLowerCase().includes(filters.searchText.toLowerCase());
	});

	document.querySelector("#notes").innerHTML = "";

	filteredNotes.forEach((note) => {
		const noteEl = generateNoteDOM(note);
		document.querySelector("#notes").appendChild(noteEl);
	});
};
